port 8080 peab olema avatud v�i vaheta porti src/main/resources/application.properties "server.port=port_you_want"

projekt asub: https://bitbucket.org/Blind0Zero/dentistapp

Esimene Viis:
	1. T�mba projekt alla
	2. ava cmd/terminal
	3. mine "cd" terminalis k�suga kausta kus dentisapp.jar on (asub Jar file kaustas)
	4. sisesta terminali : java -jar dentistapp.jar
	5. ava browser ja sisesta localhost:8080


Teine viis:
	
	1. T�mba projekt alla
	2. Ava projekt kas Eclipse v�i Intellij IDEA Community'ga
	3. Leia file DentistAppApplication and run it (src/main/java/com.cgi.dentistapp)
	4. ava browser ja sisesta localhost:8080