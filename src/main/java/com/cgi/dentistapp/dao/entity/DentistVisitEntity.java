package com.cgi.dentistapp.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dentist_visit")
public class DentistVisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = "dentist_mame")
    private String dentistName;

    @Column(name = "family_doctor_name")
    private String familyDoctorName;

    @Column(name = "visit_time")
    private Date visitTime;

    public DentistVisitEntity() {

    }

    public DentistVisitEntity(String dentistName, Date visitTime) {
        this.setDentistName(dentistName);
        this.setVisitTime(visitTime);
    }


    public DentistVisitEntity(Long id,String dentistName, Date visitTime) {
        this.setId(id);
        this.setDentistName(dentistName);
        this.setVisitTime(visitTime);
    }
    public DentistVisitEntity(String dentistName, String familyDoctorName, Date visitTime) {
        this.setDentistName(dentistName);
        this.setFamilyDoctorName(familyDoctorName);
        this.setVisitTime(visitTime);
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getDentistName() { return dentistName; }

    public void setDentistName(String dentistName) { this.dentistName = dentistName; }

    public String getFamilyDoctorName() { return familyDoctorName; }

    public void setFamilyDoctorName(String familyDoctorName) {
        this.familyDoctorName = familyDoctorName;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

}
