package com.cgi.dentistapp.dao.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FamilyDoctorEntity {

    @Id
    private int id;

    public String familyDoctorNameT;

    public FamilyDoctorEntity(){}

    public FamilyDoctorEntity(int id, String familyDoctorNameT) {
        this.id = id;
        this.familyDoctorNameT = familyDoctorNameT;
    }

    public String getFamilyDoctornameT() { return familyDoctorNameT; }

    public void setFamilyDoctornameT(String familyDoctorNameT) {
        this.familyDoctorNameT = familyDoctorNameT;
    }

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }
}
