package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.util.Date;
import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
    }

    public void delete(Long id){
        entityManager.createQuery("DELETE FROM DentistVisitEntity e where e.id = :id")
                .setParameter("id", id).executeUpdate();
    }

    public void edit(DentistVisitEntity visit){
        Long id = visit.getId();
        String dentistName = visit.getDentistName();
        Date visitTime = visit.getVisitTime();
        entityManager.createQuery(
                "UPDATE DentistVisitEntity e SET e.dentistName = ?0, e.visitTime = ?1 WHERE e.id = ?2")
                .setParameter(0 , dentistName)
                .setParameter(1 , visitTime)
                .setParameter(2 , id)
                .executeUpdate();
    }

    public List<DentistVisitEntity> getVisitsByName(String name){
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE dentistName = :name")
                .setParameter("name", name).getResultList();
    }
}
