package com.cgi.dentistapp.dao;

import com.cgi.dentistapp.dao.entity.FamilyDoctorEntity;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FamilyDoctorDao {

    private static Map<Integer, FamilyDoctorEntity> familyDoctors;

    static{
        familyDoctors = new HashMap<Integer, FamilyDoctorEntity>(){
            {
                put(1, new FamilyDoctorEntity(1, "Dr One"));
                put(2, new FamilyDoctorEntity(2, "Dr Two"));
                put(3, new FamilyDoctorEntity(3, "Dr Three"));
                put(4, new FamilyDoctorEntity(4, "Dr Four"));
                put(5, new FamilyDoctorEntity(5, "Dr Five"));
            }
        };
    }

    public Collection<FamilyDoctorEntity> getAllFamilyDoctors(){
        return this.familyDoctors.values();
    }
}
