package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.FamilyDoctorVisitService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class DentistController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;
    @Autowired
    private FamilyDoctorVisitService familyDoctorVisitService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO, HttpServletRequest request) {
        request.setAttribute("familyDoctorEntitys", familyDoctorVisitService.listAllFamilyDoctors());
        return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult,
                                   HttpServletRequest request) {
        request.setAttribute("familyDoctorEntitys", familyDoctorVisitService.listAllFamilyDoctors());
        if (bindingResult.hasErrors()) {
            return "form";
        }
        dentistVisitService.addVisit(dentistVisitDTO.getDentistName(),dentistVisitDTO.getFamilyDoctorName()
                , dentistVisitDTO.getVisitTime());
        return "redirect:/results";
    }
    @GetMapping("/results")
    public String searchList(@RequestParam (value = "search", required = false) String name, HttpServletRequest request){
        request.setAttribute("searchName", dentistVisitService.listVisitsByName(name));
        return "results";
    }

    @GetMapping("/list")
    public String populateList(HttpServletRequest request){
        request.setAttribute("dentistVisitEntitys", dentistVisitService.listVisits());
        return "list";
    }

    @GetMapping("/delete_dentist/{id}")
    public String deleteDentistVisit(@PathVariable Long id, HttpServletRequest request){
        dentistVisitService.delete(id);
        request.setAttribute("dentistVisitEntitys", dentistVisitService.listVisits());
        return "list";
    }

    @GetMapping("edit_dentist/{id}")
    public String showEditForm(DentistVisitDTO dentistVisitDTO) {
        return "editForm";
    }

    @PostMapping("edit_dentist/{id}")
    public String editDentistVisit(@PathVariable Long id, @Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "edit_dentist/{id}";
        }

        DentistVisitEntity visit = new DentistVisitEntity(id, dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime());
        dentistVisitService.edit(visit);
        return "redirect:/results";
    }
}
