package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by serkp on 2.03.2017.
 */
public class DentistVisitDTO {

    @Size(min = 1, max = 50)
    String dentistName;

    String familyDoctorName;
    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy H.mm")
    Date visitTime;

    public DentistVisitDTO() {
    }

    public DentistVisitDTO(String dentistName,  Date visitTime) {
        this.dentistName = dentistName;
        this.visitTime = visitTime;
    }
    public DentistVisitDTO(String dentistName, String familyDoctorName, Date visitTime) {
        this.dentistName = dentistName;
        this.familyDoctorName = familyDoctorName;
        this.visitTime = visitTime;
    }

    public String getDentistName() {
        return dentistName;
    }

    public void setDentistName(String dentistName) {
        this.dentistName = dentistName;
    }

    public String getFamilyDoctorName() { return familyDoctorName; }

    public void setFamilyDoctorName(String familyDoctorName) {
        this.familyDoctorName = familyDoctorName;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }
}
