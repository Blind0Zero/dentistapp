package com.cgi.dentistapp.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.cgi.dentistapp.dao.entity.FamilyDoctorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    public void addVisit(String dentistName,String familyDoctorName, Date visitTime) {
        DentistVisitEntity visit = new DentistVisitEntity(dentistName, familyDoctorName, visitTime);
        dentistVisitDao.create(visit);
    }

    public List<DentistVisitEntity> listVisits () {
        return dentistVisitDao.getAllVisits();
    }

    public List<DentistVisitEntity> listVisitsByName(String name){ return dentistVisitDao.getVisitsByName(name);}

    public void delete(Long id){dentistVisitDao.delete(id);}

    public void edit(DentistVisitEntity visit){dentistVisitDao.edit(visit);}


}
