package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dao.FamilyDoctorDao;
import com.cgi.dentistapp.dao.entity.FamilyDoctorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class FamilyDoctorVisitService {

    @Autowired
    private FamilyDoctorDao familyDoctorDao;

    public Collection<FamilyDoctorEntity> listAllFamilyDoctors(){
        return familyDoctorDao.getAllFamilyDoctors();
    }
}
